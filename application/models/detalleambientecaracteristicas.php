<?php
class Detalleambientecaracteristicas extends CI_Model{
	function __construct(){
		parent::__construct();
	}
	function getAll(){
		$this->db->where('BEstado', '1');
		$query = $this->db->get('detalleambientecaracteristicas');		
		//$query = $this->db->query("SELECT * FROM detalleambientecaracteristicas WHERE BEstado = 1;");
		return $query->result_array();
	}
	public function getById($id)
	{
		$this->db->where('BEstado', '1');
		$this->db->where('IdDetalleCarateristicas',$id);
		$query = $this->db->get('detalleambientecaracteristicas');
		return $query->row();
	}
	public function getByIdAmbiente($id)
	{
		$this->db->where('BEstado', '1');
		$this->db->where('IdAmbiente',$id);
		$query = $this->db->get('detalleambientecaracteristicas');
		return $query->row();

		$this->db->select('*');
		$this->db->from('detalleambientecaracteristicas');
		$this->db->join('caracteristicadeportivas', 'caracteristicadeportivas.IdCaracteristicaDeportivas = detalleambientecaracteristicas.IdCaracteristicasDeportivas');
		$query = $this->db->get();
	}
	public function insert($data)//recibe un objeto
	{
		$this->db->insert('detalleambientecaracteristicas', $data);
	}
	/*public function cleanAll()
	{
		$this->db->empty_table('detalleambientecaracteristicas');
	}*/
	public function deleteById($id)
	{
		$this->db->where('IdDetalleCarateristicas', $id);
		$this->db->delete('detalleambientecaracteristicas');
	}
	public function deleteLogicById($id)
	{
		$this->db->set('BEstado', 0);
		$this->db->where('IdDetalleCarateristicas', $id);
		$this->db->update('detalleambientecaracteristicas'); // gives UPDATE mytable SET field = field+1 WHERE id = 2
	}
	public function update($data)
	{
		$this->db->replace('detalleambientecaracteristicas', $data);
		// Executes: REPLACE INTO mytable (title, name, date) VALUES ('My title', 'My name', 'My date')
	}
}
?>