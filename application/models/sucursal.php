<?php
class Sucursal extends CI_Model{
	function __construct(){
		parent::__construct();
	}
	function getAll(){
		$this->db->where('BEstado', '1');
		$query = $this->db->get('sucursal');		
		//$query = $this->db->query("SELECT * FROM sucursal WHERE BEstado = 1;");
		return $query->result_array();
	}
	public function getById($id)
	{
		$this->db->where('BEstado', '1');
		$this->db->where('IdSucursal',$id);
		$query = $this->db->get('sucursal');
		return $query->row();
	}
	public function getByIdEmpresa($id)
	{
		$this->db->where('BEstado', '1');
		$this->db->where('IdEmpresa',$id);
		$query = $this->db->get('sucursal');
		return $query->result_array();
	}
	public function insert($data)//recibe un objeto
	{
		$this->db->insert('sucursal', $data);
	}
	/*public function cleanAll()
	{
		$this->db->empty_table('sucursal');
	}*/
	public function deleteById($id)
	{
		$this->db->where('IdSucursal', $id);
		$this->db->delete('sucursal');
	}
	public function deleteLogicById($id)
	{
		$this->db->set('BEstado', 0);
		$this->db->where('IdSucursal', $id);
		$this->db->update('sucursal'); // gives UPDATE mytable SET field = field+1 WHERE id = 2
	}
	public function update($data)
	{
		$this->db->replace('sucursal', $data);
		// Executes: REPLACE INTO mytable (title, name, date) VALUES ('My title', 'My name', 'My date')
	}
}
?>