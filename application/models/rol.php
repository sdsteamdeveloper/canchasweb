<?php
class Rol extends CI_Model{
	function __construct(){
		parent::__construct();
	}
	function getAll(){
		$this->db->where('BEstado', '1');
		$query = $this->db->get('rol');		
		//$query = $this->db->query("SELECT * FROM rol WHERE BEstado = 1;");
		return $query->result_array();
	}
	public function getById($id)
	{
		$this->db->where('BEstado', '1');
		$this->db->where('IdRol',$id);
		$query = $this->db->get('rol');
		return $query->row();
	}
	public function insert($data)//recibe un objeto
	{
		$this->db->insert('rol', $data);
	}
	/*public function cleanAll()
	{
		$this->db->empty_table('rol');
	}*/
	public function deleteById($id)
	{
		$this->db->where('IdRol', $id);
		$this->db->delete('rol');
	}
	public function deleteLogicById($id)
	{
		$this->db->set('BEstado', 0);
		$this->db->where('IdRol', $id);
		$this->db->update('rol'); // gives UPDATE mytable SET field = field+1 WHERE id = 2
	}
	public function update($data)
	{
		$this->db->replace('rol', $data);
		// Executes: REPLACE INTO mytable (title, name, date) VALUES ('My title', 'My name', 'My date')
	}
}
?>