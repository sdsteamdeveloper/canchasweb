<?php
class Caracteristicadeportivas extends CI_Model{
	function __construct(){
		parent::__construct();
	}
	function getAll(){
		$this->db->where('BEstado', '1');
		$query = $this->db->get('caracteristicadeportivas');		
		//$query = $this->db->query("SELECT * FROM caracteristicadeportivas WHERE BEstado = 1;");
		return $query->result_array();
	}
	public function getById($id)
	{
		$this->db->where('BEstado', '1');
		$this->db->where('IdCaracteristicaDeportivas',$id);
		$query = $this->db->get('caracteristicadeportivas');
		return $query->row();
	}
	public function insert($data)//recibe un objeto
	{
		$this->db->insert('caracteristicadeportivas', $data);
	}
	/*public function cleanAll()
	{
		$this->db->empty_table('caracteristicadeportivas');
	}*/
	public function deleteById($id)
	{
		$this->db->where('IdCaracteristicaDeportivas', $id);
		$this->db->delete('caracteristicadeportivas');
	}
	public function deleteLogicById($id)
	{
		$this->db->set('BEstado', 0);
		$this->db->where('IdCaracteristicaDeportivas', $id);
		$this->db->update('caracteristicadeportivas'); // gives UPDATE mytable SET field = field+1 WHERE id = 2
	}
	public function update($data)
	{
		$this->db->replace('caracteristicadeportivas', $data);
		// Executes: REPLACE INTO mytable (title, name, date) VALUES ('My title', 'My name', 'My date')
	}
}
?>