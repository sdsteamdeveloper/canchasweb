<?php
class Empresa extends CI_Model{
	function __construct(){
		parent::__construct();
	}
	function getAll(){
		$this->db->where('BEstado', '1');
		$query = $this->db->get('empresa');		
		//$query = $this->db->query("SELECT * FROM empresa WHERE BEstado = 1;");
		return $query->result_array();
	}
	public function getById($id)
	{
		$this->db->where('BEstado', '1');
		$this->db->where('IdEmpresa',$id);
		$query = $this->db->get('empresa');
		return $query->row();
	}
	public function insert($data)//recibe un objeto
	{
		$this->db->insert('empresa', $data);
	}
	/*public function cleanAll()
	{
		$this->db->empty_table('empresa');
	}*/
	public function deleteById($id)
	{
		$this->db->where('IdEmpresa', $id);
		$this->db->delete('empresa');
	}
	public function deleteLogicById($id)
	{
		$this->db->set('BEstado', 0);
		$this->db->where('IdEmpresa', $id);
		$this->db->update('empresa'); // gives UPDATE mytable SET field = field+1 WHERE id = 2
	}
	public function update($data)
	{
		$this->db->replace('empresa', $data);
		// Executes: REPLACE INTO mytable (title, name, date) VALUES ('My title', 'My name', 'My date')
	}
}
?>