<?php
class Reserva extends CI_Model{
	function __construct(){
		parent::__construct();
	}
	function getAll(){
		$this->db->where('BEstado', '1');
		$query = $this->db->get('reserva');		
		//$query = $this->db->query("SELECT * FROM reserva WHERE BEstado = 1;");
		return $query->result_array();
	}
	public function getById($id)
	{
		$this->db->where('BEstado', '1');
		$this->db->where('IdReserva',$id);
		$query = $this->db->get('reserva');
		return $query->row();
	}
	public function insert($data)//recibe un objeto
	{
		$this->db->insert('reserva', $data);
	}
	/*public function cleanAll()
	{
		$this->db->empty_table('reserva');
	}*/
	public function deleteById($id)
	{
		$this->db->where('IdReserva', $id);
		$this->db->delete('reserva');
	}
	public function deleteLogicById($id)
	{
		$this->db->set('BEstado', 0);
		$this->db->where('IdReserva', $id);
		$this->db->update('reserva'); // gives UPDATE mytable SET field = field+1 WHERE id = 2
	}
	public function update($data)
	{
		$this->db->replace('reserva', $data);
		// Executes: REPLACE INTO mytable (title, name, date) VALUES ('My title', 'My name', 'My date')
	}
}
?>