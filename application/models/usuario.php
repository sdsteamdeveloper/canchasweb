<?php
class Usuario extends CI_Model{
	function __construct(){
		parent::__construct();
	}
	function getUser(){
		$this->db->where('BEstado', '1');
		$query = $this->db->get('usuario');		
		//$query = $this->db->query("SELECT * FROM usuario WHERE BEstado = 1;");
		return $query->result_array();
	}
	public function getUserById($id)
	{
		$this->db->where('BEstado', '1');
		$this->db->where('IdUsuario',$id);
		$query = $this->db->get('usuario');
		return $query->row();
	}
	public function getUserByUserPass($user, $pass)
	{
		$this->db->where('CUsuario',$user);
		$this->db->where('CClave',$pass);
		$query = $this->db->get('usuario');
		return $query->row();
	}
	public function insertusuario($usuarios)//recibe un objeto
	{
		// foreach ($usuarios as $usuario) {			
		// 	$sql = "INSERT INTO usuario(nombre,apellido,latitud,longitud) VALUES (".$this->db->escape($usuario[nombre]).", ".$this->db->escape($usuario[apellido]).", ".$this->db->escape($usuario[latitud]).", ".$this->db->escape($usuario[longitud]).")";
		// 	$this->db->query($sql);
		// }
		// echo $this->db->affected_rows();
		$this->db->insert('usuario', $usuarios);
	}
	public function eliminar()
	{
		$this->db->empty_table('usuario');
	}
	public function eliminarById($id)
	{
		$this->db->where('IdUsuario', $id);
		$this->db->delete('usuario');
	}
	public function eliminarLogicoById($id)
	{
		$this->db->set('BEstado', 0);
		$this->db->where('IdUsuario', $id);
		$this->db->update('usuario'); // gives UPDATE mytable SET field = field+1 WHERE id = 2
	}
	public function actualizar($data)
	{
		$this->db->replace('usuario', $data);
		// Executes: REPLACE INTO mytable (title, name, date) VALUES ('My title', 'My name', 'My date')
	}
}
?>