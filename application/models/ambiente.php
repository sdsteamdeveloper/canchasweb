<?php
class Ambiente extends CI_Model{
	function __construct(){
		parent::__construct();
	}
	function getAll(){
		$this->db->where('BEstado', '1');
		$query = $this->db->get('ambiente');		
		//$query = $this->db->query("SELECT * FROM ambiente WHERE BEstado = 1;");
		return $query->result_array();
	}
	public function getById($id)
	{
		$this->db->where('BEstado', '1');
		$this->db->where('IdAmbiente',$id);
		$query = $this->db->get('ambiente');
		return $query->row();
	}
	public function getByIdSucursal($id)
	{
		$this->db->where('BEstado', '1');
		$this->db->where('IdSucursal',$id);
		$query = $this->db->get('ambiente');
		return $query->row();
	}
	public function insert($data)//recibe un objeto
	{
		$this->db->insert('ambiente', $data);
	}
	/*public function cleanAll()
	{
		$this->db->empty_table('ambiente');
	}*/
	public function deleteById($id)
	{
		$this->db->where('IdAmbiente', $id);
		$this->db->delete('ambiente');
	}
	public function deleteLogicById($id)
	{
		$this->db->set('BEstado', 0);
		$this->db->where('IdAmbiente', $id);
		$this->db->update('ambiente'); // gives UPDATE mytable SET field = field+1 WHERE id = 2
	}
	public function update($data)
	{
		$this->db->replace('ambiente', $data);
		// Executes: REPLACE INTO mytable (title, name, date) VALUES ('My title', 'My name', 'My date')
	}
}
?>