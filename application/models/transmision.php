<?php
class Transmision extends CI_Model{
	function __construct(){
		parent::__construct();
	}
	function getAll(){
		$query = $this->db->get('Trasmision');
		return $query->result_array();
	}
	public function getById($id)
	{
		$this->db->where('IDTransmision',$id);
		$query = $this->db->get('Trasmision');
		return $query->row();
	}
	public function getByIdVehiculoByDateBeetween($idVehiculo, $star, $end)
	{
		$this->db->where('IDVehiculo',$idVehiculo);
		$this->db->where('Dfecreg >=',$star);
		$this->db->where('Dfecreg <=',$end);
		$query = $this->db->get('Trasmision');
		return $query->result_array();
	}
	public function getByIdVehiculoByDateBeetween2($idVehiculo, $star, $end)
	{
		$this->db->select('t.*,v.Cmarca,v.Cmodelo,v.Cplaca');
		$this->db->from('Trasmision as t');
		$this->db->join('Vehiculo as v', 'v.IDVehiculo = t.IDVehiculo', 'left');
		$this->db->where('t.IDVehiculo',$idVehiculo);
		$this->db->where('t.Dfecreg >=',$star);
		$this->db->where('t.Dfecreg <=',$end);
		$query = $this->db->get();
		return $query->result_array();
	}
	public function insert($trasmisions)//recibe un objeto
	{
		$this->db->insert('Trasmision', $trasmisions);
	}
	public function deleteAll()
	{
		$this->db->empty_table('Trasmision');
	}
	public function deleteById($id)
	{
		$this->db->where('IDTransmision', $id);
		$this->db->delete('Trasmision');
	}
	public function update($data)
	{
		$this->db->replace('Trasmision', $data);
	}
}
?>	