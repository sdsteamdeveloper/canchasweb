<?php
class Horario extends CI_Model{
	function __construct(){
		parent::__construct();
	}
	function getAll(){
		$this->db->where('BEstado', '1');
		$query = $this->db->get('horario');		
		//$query = $this->db->query("SELECT * FROM horario WHERE BEstado = 1;");
		return $query->result_array();
	}
	public function getById($id)
	{
		$this->db->where('BEstado', '1');
		$this->db->where('IdHorario',$id);
		$query = $this->db->get('horario');
		return $query->row();
	}
	public function insert($data)//recibe un objeto
	{
		$this->db->insert('horario', $data);
	}
	/*public function cleanAll()
	{
		$this->db->empty_table('horario');
	}*/
	public function deleteById($id)
	{
		$this->db->where('IdHorario', $id);
		$this->db->delete('horario');
	}
	public function deleteLogicById($id)
	{
		$this->db->set('BEstado', 0);
		$this->db->where('IdHorario', $id);
		$this->db->update('horario'); // gives UPDATE mytable SET field = field+1 WHERE id = 2
	}
	public function update($data)
	{
		$this->db->replace('horario', $data);
		// Executes: REPLACE INTO mytable (title, name, date) VALUES ('My title', 'My name', 'My date')
	}
}
?>