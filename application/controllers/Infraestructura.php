<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Infraestructura extends CI_Controller {
	function __construct()
	{
		parent::__construct();
	}
	public function index()
	{
		//$this->load->view('Infraestructura');
	}

	public function listEmpresa()
	{
		$this->load->model('empresa');
		$data = $this->empresa->getAll();
		header('Content-Type: application/json');
		echo json_encode($data);
	}

	public function getEmpresabyid()
	{
		$myid = $this->input->post('id');
		$this->load->model('empresa');
		$data = $this->empresa->getById($myid);
		header('Content-Type: application/json');
		echo json_encode($data);
	}

	public function getSucursalbyidEmpresa()
	{
		$myid = $this->input->post('id');
		$this->load->model('sucursal');
		$data = $this->sucursal->getByIdEmpresa($myid);
		header('Content-Type: application/json');
		echo json_encode($data);
	}

	public function getAmbientebyidSucursal()
	{
		$myid = $this->input->post('id');
		$this->load->model('ambiente');
		$data = $this->ambiente->getByIdSucursal($myid);
		header('Content-Type: application/json');
		echo json_encode($data);
	}

	public function getCaracteristicabyidAmbiente()
	{
		$myid = $this->input->post('id');
		$this->load->model('detalleambientecaracteristicas');
		$data = $this->detalleambientecaracteristicas->getByIdAmbiente($myid);
		header('Content-Type: application/json');
		echo json_encode($data);
	}

	public function insertEmpresa()
	{
		$data = json_decode( file_get_contents( 'php://input' ), true );
		$this->load->model('empresa');		
		$this->db->trans_begin();
		$this->empresa->insert($data);
	    if ($this->db->trans_status() === FALSE) {
	        $this->db->trans_rollback();
	        echo json_encode("Error al guardar.");
	    } else {
	        $this->db->trans_commit();
	        echo json_encode("Guardado con exito!");
	    }
	}

	public function insertSucursal()
	{
		$data = json_decode( file_get_contents( 'php://input' ), true );
		$this->load->model('sucursal');		
		$this->db->trans_begin();
		$this->sucursal->insert($data);
	    if ($this->db->trans_status() === FALSE) {
	        $this->db->trans_rollback();
	        echo json_encode("Error al guardar.");
	    } else {
	        $this->db->trans_commit();
	        echo json_encode("Guardado con exito!");
	    }
	}

	public function insertAmbiente()
	{
		$data = json_decode( file_get_contents( 'php://input' ), true );
		$this->load->model('ambiente');		
		$this->db->trans_begin();
		$this->ambiente->insert($data);
	    if ($this->db->trans_status() === FALSE) {
	        $this->db->trans_rollback();
	        echo json_encode("Error al guardar.");
	    } else {
	        $this->db->trans_commit();
	        echo json_encode("Guardado con exito!");
	    }
	}

	public function insertDetalleCaracteristica()
	{
		$data = json_decode( file_get_contents( 'php://input' ), true );
		$this->load->model('detalleambientecaracteristicas');		
		$this->db->trans_begin();
		$this->detalleambientecaracteristicas->insert($data);
	    if ($this->db->trans_status() === FALSE) {
	        $this->db->trans_rollback();
	        echo json_encode("Error al guardar.");
	    } else {
	        $this->db->trans_commit();
	        echo json_encode("Guardado con exito!");
	    }
	}

	public function insertCaracteristicaDeportiva()
	{
		$data = json_decode( file_get_contents( 'php://input' ), true );
		$this->load->model('caracteristicadeportivas');		
		$this->db->trans_begin();
		$this->caracteristicadeportivas->insert($data);
	    if ($this->db->trans_status() === FALSE) {
	        $this->db->trans_rollback();
	        echo json_encode("Error al guardar.");
	    } else {
	        $this->db->trans_commit();
	        echo json_encode("Guardado con exito!");
	    }
	}

	public function updateEmpresa()
	{
		$data = json_decode( file_get_contents( 'php://input' ), true );
		$this->load->model('empresa');
		$this->db->trans_begin();
		$this->empresa->update($data);
	    if ($this->db->trans_status() === FALSE) {
	        $this->db->trans_rollback();
	        echo json_encode("Error al guardar.");
	    } else {
	        $this->db->trans_commit();
	        echo json_encode("Guardado con exito!");
	    }
	}

	public function updateSucursal()
	{
		$data = json_decode( file_get_contents( 'php://input' ), true );
		$this->load->model('sucursal');
		$this->db->trans_begin();
		$this->sucursal->update($data);
	    if ($this->db->trans_status() === FALSE) {
	        $this->db->trans_rollback();
	        echo json_encode("Error al guardar.");
	    } else {
	        $this->db->trans_commit();
	        echo json_encode("Guardado con exito!");
	    }
	}

	public function updateAmbiente()
	{
		$data = json_decode( file_get_contents( 'php://input' ), true );
		$this->load->model('ambiente');
		$this->db->trans_begin();
		$this->ambiente->update($data);
	    if ($this->db->trans_status() === FALSE) {
	        $this->db->trans_rollback();
	        echo json_encode("Error al guardar.");
	    } else {
	        $this->db->trans_commit();
	        echo json_encode("Guardado con exito!");
	    }
	}

	public function updateDetalleCaracteristica()
	{
		$data = json_decode( file_get_contents( 'php://input' ), true );
		$this->load->model('detalleambientecaracteristicas');
		$this->db->trans_begin();
		$this->detalleambientecaracteristicas->update($data);
	    if ($this->db->trans_status() === FALSE) {
	        $this->db->trans_rollback();
	        echo json_encode("Error al guardar.");
	    } else {
	        $this->db->trans_commit();
	        echo json_encode("Guardado con exito!");
	    }
	}

	public function updateCaracteristicaDeportiva()
	{
		$data = json_decode( file_get_contents( 'php://input' ), true );
		$this->load->model('caracteristicadeportivas');
		$this->db->trans_begin();
		$this->caracteristicadeportivas->update($data);
	    if ($this->db->trans_status() === FALSE) {
	        $this->db->trans_rollback();
	        echo json_encode("Error al guardar.");
	    } else {
	        $this->db->trans_commit();
	        echo json_encode("Guardado con exito!");
	    }
	}

	public function deleteEmpresa()
	{
		$myid = $this->input->post('id');
		$this->load->model('empresa');
		$row = $this->empresa->getById($myid);
		if (!isset($row))
		{
			echo json_encode("La empresa no existe.");
			return;
		}
		$this->db->trans_begin();
		$this->empresa->deleteLogicById($myid);		
		if ($this->db->trans_status() === FALSE) {
	        $this->db->trans_rollback();
	        echo json_encode("Error al eliminar.");
	    } else {
	        $this->db->trans_commit();
	        echo json_encode("Eliminado con exito!");
	    }
	}

	public function deleteSucursal()
	{
		$myid = $this->input->post('id');
		$this->load->model('sucursal');
		$row = $this->sucursal->getById($myid);
		if (!isset($row))
		{
			echo json_encode("La sucursal no existe.");
			return;
		}
		$this->db->trans_begin();
		$this->sucursal->deleteLogicById($myid);		
		if ($this->db->trans_status() === FALSE) {
	        $this->db->trans_rollback();
	        echo json_encode("Error al eliminar.");
	    } else {
	        $this->db->trans_commit();
	        echo json_encode("Eliminado con exito!");
	    }
	}

	public function deleteAmbiente()
	{
		$myid = $this->input->post('id');
		$this->load->model('ambiente');
		$row = $this->ambiente->getById($myid);
		if (!isset($row))
		{
			echo json_encode("El ambiente no existe.");
			return;
		}
		$this->db->trans_begin();
		$this->ambiente->deleteLogicById($myid);		
		if ($this->db->trans_status() === FALSE) {
	        $this->db->trans_rollback();
	        echo json_encode("Error al eliminar.");
	    } else {
	        $this->db->trans_commit();
	        echo json_encode("Eliminado con exito!");
	    }
	}

	public function deleteDetalleCaracteristica()
	{
		$myid = $this->input->post('id');
		$this->load->model('detalleambientecaracteristicas');
		$row = $this->detalleambientecaracteristicas->getById($myid);
		if (!isset($row))
		{
			echo json_encode("El detalleambientecaracteristicas no existe.");
			return;
		}
		$this->db->trans_begin();
		$this->detalleambientecaracteristicas->deleteLogicById($myid);		
		if ($this->db->trans_status() === FALSE) {
	        $this->db->trans_rollback();
	        echo json_encode("Error al eliminar.");
	    } else {
	        $this->db->trans_commit();
	        echo json_encode("Eliminado con exito!");
	    }
	}

	public function deleteCaracteristicaDeportiva()
	{
		$myid = $this->input->post('id');
		$this->load->model('caracteristicadeportivas');
		$row = $this->caracteristicadeportivas->getById($myid);
		if (!isset($row))
		{
			echo json_encode("La caracteristicadeportivas no existe.");
			return;
		}
		$this->db->trans_begin();
		$this->caracteristicadeportivas->deleteLogicById($myid);		
		if ($this->db->trans_status() === FALSE) {
	        $this->db->trans_rollback();
	        echo json_encode("Error al eliminar.");
	    } else {
	        $this->db->trans_commit();
	        echo json_encode("Eliminado con exito!");
	    }
	}
}
?>