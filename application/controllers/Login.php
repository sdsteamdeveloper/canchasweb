<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct()
	{
		parent::__construct();
	}
	public function index()
	{
		$this->load->view('login');
	}

	public function Listuser()
	{
		$this->load->model('usuario');
		$data = $this->usuario->getUser();
		header('Content-Type: application/json');
		echo json_encode($data);
	}

	public function getuserbyid()
	{
		$myid = $this->input->post('id');
		$this->load->model('usuario');
		$data = $this->usuario->getUserById($myid);
		header('Content-Type: application/json');
		echo json_encode($data);
	}

	public function login()
	{
		$myuser = $this->input->post('user');
		$mypass = $this->input->post('pass');
		$this->load->model('usuario');
		$data = $this->usuario->getUserByUserPass($myuser, $mypass);
		header('Content-Type: application/json');
		echo json_encode($data);
	}

	public function insertuser()
	{
		$data = json_decode( file_get_contents( 'php://input' ), true );
		$this->load->model('usuario');
		
		$this->db->trans_begin();
		$this->usuario->insertusuario($data);

	    if ($this->db->trans_status() === FALSE) {
	        $this->db->trans_rollback();
	        echo json_encode("Error al guardar.");
	    } else {
	        $this->db->trans_commit();
	        echo json_encode("Guardado con exito!");
	    }
	}

	public function updateuser()
	{
		$data = json_decode( file_get_contents( 'php://input' ), true );
		$this->load->model('usuario');
		$this->db->trans_begin();
		$this->usuario->actualizar($data);
	    if ($this->db->trans_status() === FALSE) {
	        $this->db->trans_rollback();
	        echo json_encode("Error al guardar.");
	    } else {
	        $this->db->trans_commit();
	        echo json_encode("Guardado con exito!");
	    }
	}

	public function falsedeleteuser()
	{
		$myid = $this->input->post('id');
		$this->load->model('usuario');
		$row = $this->usuario->getUserById($myid);
		if (!isset($row))
		{
			echo json_encode("El usuario no existe.");
			return;
		}
		$this->db->trans_begin();
		$this->usuario->eliminarLogicoById($myid);		
		if ($this->db->trans_status() === FALSE) {
	        $this->db->trans_rollback();
	        echo json_encode("Error al eliminar.");
	    } else {
	        $this->db->trans_commit();
	        echo json_encode("Eliminado con exito!");
	    }
	}

	public function truedeleteuser()
	{
		$myid = $this->input->post('id');
		$this->load->model('usuario');
		$row = $this->usuario->getUserById($myid);
		if (!isset($row))
		{
			echo json_encode("El usuario no existe.");
			return;
		}
		$this->db->trans_begin();
		$this->usuario->eliminarById($myid);		
		if ($this->db->trans_status() === FALSE) {
	        $this->db->trans_rollback();
	        echo json_encode("Error al eliminar.");
	    } else {
	        $this->db->trans_commit();
	        echo json_encode("Eliminado con exito!");
	    }
	}
}
?>