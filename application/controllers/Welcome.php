<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$this->load->view('welcome_message');
	}

	// public function getUserById($id)//por GET
	// {
	// 	$this->load->model('usuario');
	// 	$data = $this->usuario->getUserById($id);
	// 	header('Content-Type: application/json');
	// 	echo json_encode($data);
	// }


	/*================ TRANSMISION ================*/
	public function getTransmision()
	{
		$this->load->model('transmision');
		$data = $this->transmision->getAll();
		header('Content-Type: application/json');
		echo json_encode($data);
	}

	public function getTransmisionById()//por POST
	{
		$myid = $this->input->post('id');
		$this->load->model('transmision');
		$data = $this->transmision->getById($myid);
		header('Content-Type: application/json');
		echo json_encode($data);
	}

	public function getTransmisionByIdVehiculoByDateBeetween()//por GET
	{
		$myid = $this->input->get('idVehiculo');
		$mystar = $this->input->get('inicio');
		$myend = $this->input->get('fin');
		$this->load->model('transmision');
		$data = $this->transmision->getByIdVehiculoByDateBeetween($myid, $mystar, $myend);
		header('Content-Type: application/json');
		echo json_encode($data);
	}

	public function getTransmisionByIdVehiculoByDateBeetween2()//por GET
	{
		$myid = $this->input->get('idVehiculo');
		$mystar = $this->input->get('inicio');
		$myend = $this->input->get('fin');
		$this->load->model('transmision');
		$data = $this->transmision->getByIdVehiculoByDateBeetween2($myid, $mystar, $myend);
		header('Content-Type: application/json');
		echo json_encode($data);
	}

	public function insertBatchTransmision()//por POST
	{
		$data = json_decode( file_get_contents( 'php://input' ), true );
		$this->load->model('transmision');
		// $data = $this->usuario->insertusuario($data);

		
		$this->db->trans_begin();
	    foreach((array)$data as $row) {
	       $this->transmision->insert($row);
	    }

	    if ($this->db->trans_status() === FALSE) {
	        $this->db->trans_rollback();
	        echo json_encode("Error al guardar.");
	    } else {
	        $this->db->trans_commit();
	        echo json_encode("Guardado con exito!");
	    }
	}	

	public function insertTransmision()//por POST
	{
		$data = json_decode( file_get_contents( 'php://input' ), true );
		$this->load->model('transmision');
		// $data = $this->usuario->insertusuario($data);

		
		$this->db->trans_begin();
		$this->transmision->insert($data);

	    if ($this->db->trans_status() === FALSE) {
	        $this->db->trans_rollback();
	        echo json_encode("Error al guardar.");
	    } else {
	        $this->db->trans_commit();
	        echo json_encode("Guardado con exito!");
	    }
	}

	public function insertTransmisionGET()//por GET
	{
		$mystr = $this->input->get('str');
		$data = json_decode( $mystr, true );
		$this->load->model('transmision');
		// $data = $this->usuario->insertusuario($data);

		
		$this->db->trans_begin();
		$this->transmision->insert($data);

	    if ($this->db->trans_status() === FALSE) {
	        $this->db->trans_rollback();
	        echo json_encode("Error al guardar.");
	    } else {
	        $this->db->trans_commit();
	        echo json_encode("Guardado con exito!");
	    }
	}

	public function insertTransmisionGET2()//por GET
	{
		$Dlatitud = $this->input->get('Dlatitud');
		$Dlongitud = $this->input->get('Dlongitud');
		$Daltitud = $this->input->get('Daltitud');
		$Dvelocidad = $this->input->get('Dvelocidad');
		$Dfecreg = $this->input->get('Dfecreg');
		$IDVehiculo = $this->input->get('IDVehiculo');
		$data = array(
			    "Dlatitud" => $Dlatitud,
			    "Dlongitud" => $Dlongitud,
			    "Daltitud" => $Daltitud,
			    "Dvelocidad" => $Dvelocidad,
			    "Dfecreg" => $Dfecreg,
			    "IDVehiculo" => $IDVehiculo
			);
		$this->load->model('transmision');
		
		$this->db->trans_begin();
		$this->transmision->insert($data);

	    if ($this->db->trans_status() === FALSE) {
	        $this->db->trans_rollback();
	        echo json_encode("Error al guardar.");
	    } else {
	        $this->db->trans_commit();
	        echo json_encode("Guardado con exito!");
	    }
	}

	public function actualizarTransmision()//por POST
	{
		$data = json_decode( file_get_contents( 'php://input' ), true );
		$this->load->model('transmision');

		$this->db->trans_begin();
	    $this->transmision->update($data);

	    if ($this->db->trans_status() === FALSE) {
	        $this->db->trans_rollback();
	        echo json_encode("Error al guardar.");
	    } else {
	        $this->db->trans_commit();
	        echo json_encode("Guardado con exito!");
	    }
	}

	public function deleteAllTransmision()
	{
		$this->load->model('transmision');

		$this->db->trans_begin();
		$data = $this->transmision->deleteAll();

		if ($this->db->trans_status() === FALSE) {
	        $this->db->trans_rollback();
	        echo json_encode("Error al eliminar.");
	    } else {
	        $this->db->trans_commit();
	        echo json_encode("Eliminado con exito!");
	    }
	}

	public function deleteTransmisionById()
	{
		$myid = $this->input->post('id');
		$this->load->model('transmision');

		$this->db->trans_begin();
		$data = $this->transmision->deleteById($myid);

		if ($this->db->trans_status() === FALSE) {
	        $this->db->trans_rollback();
	        echo json_encode("Error al eliminar.");
	    } else {
	        $this->db->trans_commit();
	        echo json_encode("Eliminado con exito!");
	    }
	}
}
?>